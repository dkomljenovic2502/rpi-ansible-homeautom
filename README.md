## on RPi  
install raspberryOS 64bit
##### sed this whole project `dkomljenovic` for desired user
  
Change hostname `echo "homebrain" | sudo tee /etc/hostname && sudo sed -i "s,raspberrypi,homebrain," /etc/hosts`  
Add your user e.g. dkomljenovic sudo `sudo adduser dkomljenovic && sudo usermod -aG sudo,root dkomljenovic`  
Remove the need for sudo pwd `echo "dkomljenovic ALL=(ALL) NOPASSWD:ALL"| sudo tee /etc/sudoers.d/89-homebrain-user`  
  
## on Host  
Install Ansible on host pc `sudo pacman -S ansible sshpass` [on Arch linux]  
If no pub key, generate https://help.github.com/en/github/authenticating-to-github/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent  
Copy your pub key to the device `ssh-copy-id -i ~/.ssh/id_rsa.pub dkomljenovic@raspberrypi.local`  
Install `ansible-galaxy install geerlingguy.docker_arm geerlingguy.pip`  
#### Ansible docker install  
Run `ansible-playbook 00_install_prereq.yml`  
Run `ansible-playbook 01_docker_install.yml`  
  
#### Ansible docker-containers install  
Run `ansible-playbook 02_portainer_install.yml`  
or install all `ansible-playbook 99_install_all_dockers.yml`  
...  
Install other playbooks by need  
  
## Build Node Red
go to the folder dockerfiles and run  
``` docker buildx build --platform linux/amd64,linux/arm64,linux/arm/v7 -t dkomljenovic2502/node-red-rpi -f Dockerfile-nodered . --push ```
